// Copyright (C) 2023 Nicholas Huelin. All rights reserved.

#include "Logger.hpp"

bool initialize_logging(std::string file_name) {
	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);

	// create a brand new log file
	log_file->open(file_name, std::ios::out | std::ios::trunc);

	if (!log_file->is_open()) {

		SetConsoleTextAttribute(hConsole, RED | FOREGROUND_INTENSITY);
		std::cout << "Failed to open log file: " << file_name << std::endl;
		return false;
	}

	// Start the log thread
	log_thread = std::make_unique<std::thread>(write_log_messages);
	return true;
}

void shutdown_logging() {
	// Set the closing_log_file flag to true
	closing_log_file = true;

	logging_complete = true;

	log_file->flush();
	log_file->close();
	// make sure the thread is closed
	log_thread->join();
}

void write_log_messages() {

	// Write log messages to the file until logging is complete
	while (!logging_complete || !log_queue->empty()) {
		// Lock the log queue mutex
		std::lock_guard<std::mutex> lock(*log_queue_mutex);

		// Write the log message to the file if the queue is not empty
		if (!log_queue->empty()) {
			*log_file << log_queue->front();
			log_queue->pop();
		}
	}

	// Wait for the closing_log_file flag to be set
	while (!closing_log_file) {
		std::this_thread::yield();
	}

	// Check if the log queue is empty before closing the file
	if (!log_queue->empty()) {
		// Write the remaining log messages to the file
		while (!log_queue->empty()) {
			*log_file << log_queue->front();
			log_queue->pop();
		}
	}
}

void log_message(log_level_t level, const char* message, ...) {
	static const std::string level_strings[] = {
		"[FATAL] ",
		"[ERROR] ",
		"[WARN] ",
		"[INFO] ",
		"[SUCCESS] ",
		"[DEBUG] ",
		"[TRACE] "
	};

	std::ostringstream out_message;
	{
		va_list args;
		va_start(args, message);
		std::vector<char> buffer(std::strlen(message) + 1);
		std::vsnprintf(buffer.data(), buffer.size(), message, args);
		va_end(args);
		out_message << level_strings[level] << buffer.data() << '\n';
	}
	std::string out_message2 = out_message.str();

	// change command prompt text color
	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);

	switch (level) {
	case LOG_LEVEL_FATAL:
		SetConsoleTextAttribute(hConsole, RED | FOREGROUND_INTENSITY);
		break;
	case LOG_LEVEL_ERROR:
		SetConsoleTextAttribute(hConsole, MAGENTA | FOREGROUND_INTENSITY);
		break;
	case LOG_LEVEL_WARN:
		SetConsoleTextAttribute(hConsole, YELLOW | FOREGROUND_INTENSITY);
		break;
	case LOG_LEVEL_INFO:
		SetConsoleTextAttribute(hConsole, CYAN | FOREGROUND_INTENSITY);
		break;
	case LOG_LEVEL_SUCCESS:
		SetConsoleTextAttribute(hConsole, GREEN | FOREGROUND_INTENSITY);
		break;
	case LOG_LEVEL_DEBUG:
		SetConsoleTextAttribute(hConsole, BLUE | FOREGROUND_INTENSITY);
		break;
	case LOG_LEVEL_TRACE:
		SetConsoleTextAttribute(hConsole, GRAY | FOREGROUND_INTENSITY);
		break;
	}

	printf("%s", out_message2.c_str());

	log_queue->push(out_message2);

	if (!log_thread) {
		log_thread = std::make_unique<std::thread>(write_log_messages);
	}
}
